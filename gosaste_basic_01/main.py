#from sample import graph
from readFile import readData
graph = readData()
#print(graph)

def findNext(row,path):
    minIndex=None
    l = 0
    for i in row[0]:        
        if(i not in path):
            minIndex = l
        l = l+1
    j = 0
    for i in row[0]:
        if(i not in path):
            if(row[1][j]<row[1][minIndex]):
                minIndex = j
        j = j +1
    return minIndex

def findBestPath(graph,start,path=[]):
    path.append(start)
    if(len(path) == 5):
        return path
    startNext = findNext(graph[start],path)
    start = graph[start][0][startNext]
    return findBestPath(graph,start,path)


def pathSize(graph,f,t):
    l = 0
    for i in graph[f][0]:
        if i == t:
            return int(graph[f][1][l])
        l = l + 1


def findPathSize(graph,path):
    size = 0
    for i in range(0,len(path)-1,1):
        size = size + pathSize(graph,path[i],path[i+1])
    size = size + pathSize(graph,path[0],path[len(path)-1])
    return size
    
path = findBestPath(graph,'a')
print("path is : ",path)


#40
#7 + 6 + 8 + 5 +14

size = findPathSize(graph,path)
print("and size is : ",size)
