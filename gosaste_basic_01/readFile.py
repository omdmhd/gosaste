def readData():
    myFile = open("information.txt","r",1)
    data = myFile.readline()
    
    items = []
    for item in data.strip().split(" "):
        items.append(item)
    graph={}
    j = 0
    for line in myFile.readlines():
        graph[items[j]] = [[],[]]
        i = 0
        for item in line.strip().split("  "):
            item = int(item)
            if(item != 0):
                #print(items[i])
                #print(item)
                graph[items[j]][0].append(items[i])
                graph[items[j]][1].append(item)
            i = i + 1
        #print("-------")
        j = j + 1
    return graph

